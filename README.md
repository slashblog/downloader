# Downloader

Enables background downloading of Videos supported by [youtube-dl](https://youtube-dl.org/).

## Requirements
1. youtube-dl
2. aria2c
3. torsocks (optional)

## Getting started

1. Install the above packages. 
2. In the cloned directory, execute the command: `mkdir -p completed incoming list proxy-list`
3. For any video supported by youtube-dl, create a file will file name same as url of video in the *list* directory. For example, `touch list/$url`.
4. If you want to use torsocks to download the video, create the file in *proxy-list* directory. For example, `touch proxy-list/$url`. Note torsocks needs to be configured already to download the videos.
5. For, youtube url you can create a file with only youtube video-id. For example, `touch list $videoid`.

### Starting download

To start the actual download process create a recurring cronjob that will execute the downloader script. *downloader* script checks if there are any files in *list* and *proxy-list* directories, and downloads the corresponding videos. If there are no such files it does not do anything. 

`downloader` script takes a final destination directory where the finished videos are downloaded. Further note that, the videos will be downloaded in *incoming* directory, so it needs to have the required disk space.

To create cron job execute the command `cronjob -e`, and add the following lines (after making the necessary modifications): 
```
*/5 * * * * ${WORKDIR}/downloader/downloader ${DESTINATION} >> ${WORKDIR}/downloader/downloader.log 2>&1
0 3 * * * if [ $(ls -1 ${WORKDIR}/downloader/list | wc -l) -eq 0 ] ; then rm -vf ${WORKDIR}/downloader/downloader.log ; fi
```
The above cron entries create two cron jobs. Replace *${WORKDIR}* with full path to the cloned directory, i.e. downloader directory, and replace *${DESTINATION}* with the directory where you want the final downloaded video files to reside. Details of above cron entries

1. Execute `downloader` script every five minutes.
2. Delete *downloader.log* file everyday at 3 AM.

## Logs
Logs are present in *downloader.log* file.
